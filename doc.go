// Package livefile implements atomic file update operations,
// as well as consistent, safe real-time notifications for file changes.
package livefile
