package livefile_test

import (
	"fmt"
	"io/fs"
	"os"
	"strings"
	"sync"
	"testing"
	"time"

	"gitlab.com/xtgo/livefile"
)

func TestWatcher(t *testing.T) {
	cwd, err := os.Getwd()
	if err != nil {
		t.Skip("unable to find current directory:", err)
	}

	t.Cleanup(func() { os.Chdir(cwd) })

	tests := []struct {
		name        string
		backup      bool
		restore     bool
		callOnWatch bool
		steps       []operation
	}{
		{
			name:        "watch_existing_file",
			callOnWatch: true,
			steps: []operation{
				update("filename", 0600, "filedata"),
				watch("filename", func(string) bool { return true }),
				assertCallbackSuccess("filename"),
				assertRegular("filename", 0600, "filedata"),
			},
		},
		{
			name: "watch_nonexistant_file",
			steps: []operation{
				watch("filename", func(string) bool { return true }),
				update("filename", 0600, "filedata"),
				assertCallbackSuccess("filename"),
				assertRegular("filename", 0600, "filedata"),
			},
		},
		{
			name:        "watch_existing_file_backup",
			backup:      true,
			callOnWatch: true,
			steps: []operation{
				update("filename", 0600, "filedata"),
				watch("filename", func(string) bool { return true }),
				assertCallbackSuccess("filename"),
				assertRegular("filename", 0600, "filedata"),
				assertRegular("filename~", 0600, "filedata"),
			},
		},
		{
			name:        "watch_nonexistant_file_backup",
			backup:      true,
			callOnWatch: true,
			steps: []operation{
				watch("filename", func(string) bool { return true }),
				assertCallbackSuccess("filename"),
				assertMissing("filename"),
				assertMissing("filename~"),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir := t.TempDir()

			err := os.Chdir(dir)
			if err != nil {
				t.Skip("unable to change directory:", err)
			}

			w := livefile.Watcher{
				Backup:      tt.backup,
				Restore:     tt.restore,
				CallOnWatch: tt.callOnWatch,

				OnError: func(err error) {
					t.Fatal("async error:", err)
				},
			}

			defer w.Close()

			tc := testContext{
				watcher: &w,
				events:  make(map[string]chan bool),
			}

			for _, step := range tt.steps {
				err := step(&tc)
				if err != nil {
					t.Fatal(err)
				}
			}
		})
	}
}

type testContext struct {
	watcher *livefile.Watcher
	mu      sync.RWMutex
	events  map[string]chan bool
}

type operation func(tc *testContext) error

func assertCallbackSuccess(path string) operation {
	return assertCallback(path, true)
}

func assertCallbackFailure(path string) operation {
	return assertCallback(path, false)
}

func assertCallback(path string, want bool) operation {
	return func(tc *testContext) error {
		const timeout = 10 * time.Second

		tc.mu.RLock()
		ch := tc.events[path]
		tc.mu.RUnlock()

		select {
		case <-time.After(timeout):
			return fmt.Errorf("timeout exceeded waiting for inotify event")

		case got := <-ch:
			if got == want {
				return nil
			}

			msg := "callback on %q returned %v, want %v"

			return fmt.Errorf(msg, path, got, want)
		}
	}
}

func assertMissing(path string) operation {
	return func(tc *testContext) error {
		_, err := os.Lstat(path)
		if err == nil {
			return os.ErrExist
		}

		if os.IsNotExist(err) {
			return nil
		}

		return err
	}
}

func assertRegular(path string, perm fs.FileMode, data string) operation {
	return func(tc *testContext) error {
		const (
			timeout  = 1500 * time.Millisecond
			interval = 25 * time.Millisecond
		)

		deadline := time.Now().Add(timeout)

		var info os.FileInfo

		for {
			var err error

			info, err = os.Lstat(path)
			if err == nil {
				break
			}

			if !os.IsNotExist(err) || time.Now().After(deadline) {
				return err
			}

			time.Sleep(interval)
		}

		mode := info.Mode()
		if !mode.IsRegular() {
			return fmt.Errorf("%s is not a regular file", path)
		}

		mode = mode.Perm()
		if mode != perm {
			return fmt.Errorf("%s has perm %v, want %v", path, mode, perm)
		}

		buf, err := os.ReadFile(path)
		if err != nil {
			return err
		}

		if string(buf) != data {
			return fmt.Errorf("%s has content %#q, want %#q", path, buf, data)
		}

		return nil
	}
}

func watch(path string, callback livefile.Callback) operation {
	return func(tc *testContext) error {
		tc.mu.Lock()
		defer tc.mu.Unlock()

		ch := tc.events[path]
		if ch == nil {
			ch = make(chan bool)
			tc.events[path] = ch
		}

		return tc.watcher.Watch(path,
			func(key string) bool {
				ok := callback(key)
				ch <- ok

				return ok
			})
	}
}

func mkdir(path string) operation {
	return func(*testContext) error {
		return os.Mkdir(path, 0700)
	}
}

func rename(src, dst string) operation {
	return func(*testContext) error {
		return os.Rename(src, dst)
	}
}

func update(path string, mode fs.FileMode, data string) operation {
	return func(*testContext) error {
		return livefile.Update(path, mode, strings.NewReader(data))
	}
}

func symlink(src, dst string) operation {
	return func(tc *testContext) error {
		return livefile.Symlink(src, dst)
	}
}

func unlink(path string) operation {
	return func(tc *testContext) error {
		return livefile.Remove(path)
	}
}
