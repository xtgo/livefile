package livefile

import (
	"crypto/rand"
	"encoding/hex"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

// Remove unlinks the file at path.
func Remove(path string) error {
	return os.Remove(path)
}

// Symlink atomically replaces the file at dst
// with a symlink pointing to the given src.
func Symlink(src, dst string) error {
	return copySymlink(src, dst)
}

// Update atomically replaces the contents at path with the given Reader data.
func Update(path string, perm fs.FileMode, r io.Reader) error {
	dir, pattern := makeDirPattern(path)

	dstFile, err := os.CreateTemp(dir, pattern)
	if err != nil {
		return err
	}

	defer os.Remove(dstFile.Name())
	defer dstFile.Close()

	_, err = io.Copy(dstFile, r)
	if err != nil {
		return err
	}

	err = dstFile.Sync()
	if err != nil {
		return err
	}

	if perm != 0600 {
		err = dstFile.Chmod(perm)
		if err != nil {
			return err
		}
	}

	err = dstFile.Close()
	if err != nil {
		return err
	}

	return os.Rename(dstFile.Name(), path)
}

func copyPath(srcPath, dstPath string) error {
	info, err := os.Lstat(srcPath)
	if err != nil {
		return err
	}

	if info.Mode()&fs.ModeSymlink != 0 {
		return copySymlink(srcPath, dstPath)
	}

	srcFile, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	return Update(dstPath, info.Mode().Perm(), srcFile)
}

func copySymlink(srcPath, dstPath string) error {
	dir, pattern := makeDirPattern(dstPath)

	tmpPath, err := createTemp(dir, pattern, func(path string) error {
		return os.Symlink(srcPath, path)
	})

	if err != nil {
		return err
	}

	defer os.Remove(tmpPath)

	return os.Rename(tmpPath, dstPath)
}

func backupPath(path string) string {
	return path + "~"
}

func makeDirPattern(path string) (dir, pattern string) {
	dir = filepath.Dir(path)
	base := filepath.Base(path)

	prefix := "."
	suffix := "~"

	if strings.HasPrefix(base, prefix) {
		prefix = ""
	}

	if strings.HasSuffix(base, suffix) {
		suffix = ""
	}

	return dir, prefix + base + suffix
}

// createTemp is used for atomic symlink creation and other special cases,
// returning the temporary path and an error.
//
func createTemp(dir, prefix string, fn func(path string) error) (string, error) {
	// the stdlib CreateTemp implementation tries 10k times
	const tries = 10000

	// entropy
	const k = 8

	const sep = string(filepath.Separator)

	// path prefix + random hex suffix
	n := len(dir) + len(sep) + len(prefix) + k + k

	buf := make([]byte, 0, n)
	buf = append(buf, dir...)
	buf = append(buf, sep...)
	buf = append(buf, prefix...)
	buf = buf[:n]

	var err error

	for try := 0; ; try++ {
		_, err = rand.Read(buf[n-k:])
		if err != nil {
			return "", err
		}

		// encode n bytes in to 2n hex-chars, both at tail of buffer
		hex.Encode(buf[n-k-k:], buf[n-k:])
		path := string(buf)

		err = fn(path)
		if err == nil {
			return path, nil
		}

		if try >= tries || !os.IsExist(err) {
			return "", err
		}
	}
}
