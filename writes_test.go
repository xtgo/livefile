package livefile

import (
	"os"
	"regexp"
	"testing"
)

func Test_createTemp(t *testing.T) {
	regex := regexp.MustCompile(`^/path/to/dir/some-file~[0-9a-f]{16}$`)

	const dir = "/path/to/dir"
	const pre = "some-file~"

	tries := 5
	var lastPath string

	path, err := createTemp(dir, pre, func(path string) error {
		if path == lastPath {
			msg := "createTemp used same path twice:\nprev: %q\ncurr: %q"
			t.Fatalf(msg, lastPath, path)
		}

		lastPath = path

		if !regex.MatchString(path) {
			msg := "createTemp produced non-conforming path: %q"
			t.Fatalf(msg, path)
		}

		if tries == 0 {
			return nil
		}

		tries--

		return os.ErrExist
	})

	if err != nil {
		msg := "createTemp(...) = %q [error], want nil"
		t.Fatalf(msg, err)
	}

	if path != lastPath {
		msg := "createTemp(...) = %q, want: %q"
		t.Fatalf(msg, path, lastPath)
	}

	if tries != 0 {
		msg := "createTemp took fewer than expected tries (got %d, want 0)"
		t.Fatalf(msg, tries)
	}
}
