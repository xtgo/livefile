package livefile

import (
	"os"
	"path/filepath"
	"sync"

	"github.com/sigma/go-inotify"
)

// Watcher monitors filesystem paths registered with Watch,
// invoking the supplied path-specific callback any time the file
// has been written and closed or atomically updated.
//
// All registered callbacks are called synchronously
// in the order of file changes.
// If asynchronous dispatch is required,
// invoke a goroutine within a callback.
//
// The zero value is ready to use.
// Configuration field updates should occur prior to the first call to Watch.
//
type Watcher struct {
	// Backup controls whether changes are
	// When zero, no backup or file restoration will occur;
	// when non-zero, read files will be written with a '~' filename suffix.
	Backup bool

	// Restore controls whether rejected changes are overwritten from backup,
	// whenever a backup is present.
	Restore bool

	// CallOnWatch controls whether a callback will be invoked,
	// as a convenience, at the time Watch is called.
	CallOnWatch bool

	// OnError, if non-nil, is called when asynchronous errors are detected.
	OnError func(err error)

	mu      sync.Mutex
	done    <-chan struct{} // closed when monitor goroutine exits
	watc    chan<- watch
	dirs    map[string]struct{}
	inotify *inotify.Watcher
}

// Callback represents any function that can react to file change events.
// If the function returns false, the originating change will be rejected
// and if backups are enabled, previous file contents will be restored.
// Any state that is updated should be done so atomically
// after validation has been performed.
//
type Callback func(path string) bool

// Watch will monitor the given file path
// invoking callback whenever the file content has changed.
// Watch will return an error if the same filename is passed multiple times,
// or if file monitoring could not be initiated.
//
// If callback is nil, the path will no longer be watched.
//
// Otherwise, if Watcher.CallOnWatch is true,
// the given callback will be asynchronously invoked following this call.
//
func (w *Watcher) Watch(path string, callback Callback) error {
	absPath, err := filepath.Abs(path)
	if err != nil {
		return err
	}

	wat := watch{
		origPath: path,
		absPath:  absPath,
		callback: callback,
	}

	err = w.watchDir(wat)
	if err != nil {
		return err
	}

	w.watc <- wat

	return nil
}

// Close causes the receiver to stop monitoring paths
// and releases associated operating system resources.
//
// Close will block until remaining events have been consumed.
func (w *Watcher) Close() error {
	w.mu.Lock()
	defer w.mu.Unlock()

	if w.inotify == nil {
		return nil
	}

	done := w.done
	ino := w.inotify

	defer func() { <-done }()
	defer close(w.watc)

	w.done = nil
	w.watc = nil
	w.dirs = nil
	w.inotify = nil

	return ino.Close()
}

func (w *Watcher) init() error {
	if w.inotify != nil {
		return nil
	}

	ino, err := inotify.NewWatcher()
	if err != nil {
		return err
	}

	done := make(chan struct{})
	watc := make(chan watch)

	w.done = done
	w.watc = watc
	w.dirs = make(map[string]struct{})
	w.inotify = ino

	go w.monitor(done, watc, w.inotify.Event)

	return nil
}

func (w *Watcher) watchDir(wat watch) error {
	dir := filepath.Dir(wat.absPath)

	w.mu.Lock()
	defer w.mu.Unlock()

	err := w.init()
	if err != nil {
		return err
	}

	if wat.callback == nil {
		return nil
	}

	_, ok := w.dirs[dir]
	if ok {
		return nil
	}

	const flags = inotify.IN_ONLYDIR |
		inotify.IN_MOVE |
		inotify.IN_DELETE |
		inotify.IN_CLOSE_WRITE

	err = w.inotify.AddWatch(dir, flags)
	if err != nil {
		return err
	}

	w.dirs[dir] = struct{}{}

	return nil
}

type watch struct {
	origPath string
	absPath  string
	callback Callback
}

func (w *Watcher) monitor(done chan<- struct{}, watc <-chan watch, eventc <-chan *inotify.Event) {
	defer close(done)

	watches := make(map[string]watch)

	for {
		select {
		case event, ok := <-eventc:
			if !ok {
				return
			}

			wat := watches[event.Name]
			if wat.callback != nil {
				w.handle(wat)
			}

		case wat, ok := <-watc:
			if !ok {
				return
			}

			if wat.callback == nil {
				delete(watches, wat.absPath)
				continue
			}

			watches[wat.absPath] = wat

			if w.CallOnWatch {
				w.handle(wat)
			}
		}
	}
}

func (w *Watcher) handle(wat watch) {
	if wat.origPath == "" || wat.callback == nil {
		return
	}

	ok := wat.callback(wat.origPath)

	var err error

	if ok && w.Backup {
		err = copyPath(wat.absPath, backupPath(wat.absPath))
	} else if !ok && w.Restore {
		err = copyPath(backupPath(wat.absPath), wat.absPath)
	}

	if os.IsNotExist(err) {
		// for backup and restore operations, treat a non-existant
		// source path as a non error:
		// there's nothing to backup if the callback accepted a path
		// to a non-existant file, and there's nothing to restore
		// if the backup doesn't exist.
		err = nil
	}

	if err != nil && w.OnError != nil {
		w.OnError(err)
	}
}
